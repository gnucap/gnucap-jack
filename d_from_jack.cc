/*                      -*- C++ -*-
 * Copyright (C) 2013-2016 Felix Salfelder
 * Author: Felix Salfelder <felix@salfelder.org>
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 */

#include <globals.h>
#include <e_compon.h>
#include <e_node.h>
#include "e_jack.h"
/*--------------------------------------------------------------------------*/
namespace {
/*--------------------------------------------------------------------------*/
class DEV_V_JACK : public DEV_JACK {

  DEV_V_JACK(DEV_V_JACK const& p) : DEV_JACK(p) { untested();
  }
#if 0
	CARD* clone()const override { incomplete();
			const CARD* c = device_dispatcher["V"];
			assert(c);
			CARD* c2 = c->clone();
			COMPONENT* d = prechecked_cast<COMPONENT*>(c2);
			assert(d);
			const COMMON_COMPONENT* b = bm_dispatcher["jack"];
			assert(b);
			COMMON_COMPONENT* bc = b->clone();
			d->attach_common(bc);
	//		assert(d->dev_type() == "v_jack"); // not yet
			return d;
		}
#endif
public:
  DEV_V_JACK() : DEV_JACK() {
    attach_common(&bmjack);
    set_dev_type("v_jack");
  }
	std::string value_name()const override { untested(); return "dummy"; }
	std::string port_name(int)const override { untested(); return "dummy"; }
	void tr_accept()override { untested();
		assert(_buffer);
		_pos += _samples_per_step;
	}
	// TODO: vsrc.

  bool print_type_in_spice()const override { untested();return false;}


  char	   id_letter()const override	{ untested();return 'V';}
  int	   max_nodes()const override	{ untested();return 2;}
  int	   min_nodes()const override	{ untested();return 2;}
  int	   matrix_nodes()const override	{ untested();return 2;}
  int	   net_nodes()const override	{ untested();return 2;}
  bool	   is_source()const override	{ untested();return true;}
  bool	   f_is_value()const override	{ untested();return true;}
  bool	   has_iv_probe()const override	{ untested();return true;}
  bool	   use_obsolete_callback_parse()const override { untested();return true;}
  CARD*	   clone()const override	{ untested();return new DEV_V_JACK(*this);}
  void     precalc_last()override;
  void     dc_advance()override;
  void	   tr_iwant_matrix()override	{ untested();tr_iwant_matrix_passive();}
  void	   tr_begin()override;
  bool	   do_tr()override;
 // void	   tr_eval();
  void	   tr_load()override		{ untested();tr_load_shunt(); tr_load_source();}
  void	   tr_unload()override		{ untested();tr_unload_source();}
  double   tr_involts()const override	{ untested();return 0.;}
  double   tr_involts_limited()const override{ untested();unreachable(); return 0.;}
  void	   ac_iwant_matrix()override	{ untested();ac_iwant_matrix_passive();}
  void	   ac_begin()override		{ untested();_loss1 = _loss0 = 1./OPT::shortckt; _acg = _ev = 0.;}
  void	   do_ac()override{ untested();incomplete();}
  void	   ac_load()override		{ untested();ac_load_shunt(); ac_load_source();}
  COMPLEX  ac_involts()const override	{ untested();return 0.;}
  COMPLEX  ac_amps()const override	{ untested();return (_acg + ac_outvolts()*_loss0);}
}p2;
/*--------------------------------------------------------------------------*/
void DEV_V_JACK::precalc_last()
{ untested();
  ELEMENT::precalc_last();
  set_constant(!using_tr_eval());
  set_converged(!has_tr_eval());
}
/*--------------------------------------------------------------------------*/
void DEV_V_JACK::dc_advance()
{ untested();
  ELEMENT::dc_advance();

  if(using_tr_eval()){ untested();
  }else{ untested();
    _y[0].f1 = value();
    if(_y[0].f1 != _y1.f1){ untested();
      store_values();
      q_load();
      _m0.c0 = -_loss0 * _y[0].f1;
      // set_constant(false); not needed. nothing to do in do_tr.
    }else{ untested();
    }
  }
}
/*--------------------------------------------------------------------------*/
void DEV_V_JACK::tr_begin()
{ untested();
  ELEMENT::tr_begin();
  _y[0].x  = 0.;
  _y[0].f1 = value();
  _y1.f0 = _y[0].f0 = 0.;	//BUG// override
  _loss1 = _loss0 = 1./OPT::shortckt;
  _m0.x  = 0.;
  _m0.c0 = -_loss0 * _y[0].f1;
  _m0.c1 = 0.;
  _m1 = _m0;
  if (!using_tr_eval()) { untested();
    if (n_(OUT2).m_() == 0) { untested();
      _sim->set_limit(value());
    }else if (n_(OUT1).m_() == 0) { untested();
      _sim->set_limit(-value());
    }else{ untested();
      //BUG// don't set limit
    }
  }else{ untested();
  }
}
/*--------------------------------------------------------------------------*/
bool DEV_V_JACK::do_tr()
{ untested();
  assert(_m0.x == 0.);
  if (using_tr_eval()) { untested();
    _y[0].x = _sim->_time0;
    tr_eval();
    if (n_(OUT2).m_() == 0) { untested();
      _sim->set_limit(_y[0].f1);
    }else if (n_(OUT1).m_() == 0) { untested();
      _sim->set_limit(-_y[0].f1);
    }else{ untested();
      //BUG// don't set limit
    }
    store_values();
    q_load();
    _m0.c0 = -_loss0 * _y[0].f1;
    assert(_m0.c1 == 0.);
  }else{itested();
    assert(conchk(_loss0, 1./OPT::shortckt));
    assert(_y[0].x == 0.);
    assert(_y[0].f0 == 0.);
    assert(_y[0].f1 == value());
    assert(_m0.x == 0.);
    assert(conchk(_m0.c0, -_loss0 * _y[0].f1));
    assert(_m0.c1 == 0.);
    assert(_y1 == _y[0]);
    assert(converged());
  }
  return converged();
}
/*--------------------------------------------------------------------------*/
DISPATCHER<CARD>::INSTALL d2(&device_dispatcher, "from_jack|v_jack", &p2);
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
// vim:ts=8:sw=2:noet:
