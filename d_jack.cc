/*                              -*- C++ -*-
 * Copyright (C) 2024 Albert Davis
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 */
extern "C"{
  #include <jack/jack.h>
  #include <jack/transport.h>
  #include <sys/types.h>
  #include <unistd.h>
}

#include <globals.h>
#include "e_jack.h"
#include <u_xprobe.h>
/*--------------------------------------------------------------------------*/
unsigned long DEV_JACK::_sr;
unsigned DEV_JACK::_nframes;
unsigned short DEV_JACK::_samples_per_step;
DEV_JACK* DEV_JACK::_ctx[MAXPORTS];
jack_client_t* DEV_JACK::_client;
/*--------------------------------------------------------------------------*/
void DEV_JACK::expand()
{
	ELEMENT::expand();
	auto d = this; // prechecked_cast<DEV_JACK const*>(D);
	assert(d);
	// trace3("expand", d->long_label(), hp(this), hp(d));
	pid_t p = getpid();
	std::string client_name = "gnucap [" + to_string(p) + "]";
	const char* port_name = d->long_label().c_str();

	if (_client) { untested();
	} else {
		if ((_client = jack_client_open (client_name.c_str(), JackNullOption, 0)) == 0) {
			fprintf (stderr, "jack server not running?\n");
			throw Exception("jack server not running?");
		}
		_sr = jack_get_sample_rate(_client);
		_nframes = jack_get_buffer_size(_client);

		error(bTRACE, "nframes: " + to_string(_nframes) + "\n");
		error(bTRACE, "samplerate " + to_string((int)_sr) + "\n");
		error(bTRACE, "dtmin " + to_string(d->_sim->_dtmin) + "\n");

		jack_set_sample_rate_callback(_client, srate_cb, (void*) _ctx);
		jack_set_process_callback (_client, process_cb, (void*) _ctx);
	}

	assert(_nframes);
	if (_buffer) {
	}else{
		_buffer = new sample_t[_nframes];
	}

	if (jack_activate (_client)) {
		fprintf (stderr, "cannot activate client\n");
		throw Exception("cannot activate");
	}


	if (_jack_port) {
	}else{
		std::string dir;
		if (d->is_source()) {
			_jack_port = jack_port_register (_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
			dir = "input";
			memset (_buffer, 0, sizeof (sample_t) * _nframes);
		}else{
			_jack_port = jack_port_register (_client, port_name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
			dir = "output";
		}
		DEV_JACK** c = _ctx;
		while (*c) {
			++c;
		}
		error(bTRACE, "registering " + dir + " port " + d->long_label() + " in slot " + to_string(int(c-_ctx)) + "\n");
		assert(c-_ctx<MAXPORTS);
		*c = this;

		auto cj = prechecked_cast<EVAL_BM_JACK const*>(common());
		assert(cj);
		if (cj->_connect && _jack_port) {
			error(bDEBUG, "connecting " + d->long_label() + " to " + to_string(cj->_connect) + "\n");
			const char ** ports;
			if ((ports = jack_get_ports (_client, NULL, NULL,
							JackPortIsPhysical|JackPortIsInput)) == NULL) {
				fprintf(stderr, "Cannot find any physical playback ports\n");
				throw Exception("cannot connect" + d->long_label());
			}

			int c = 0;
			while (c+1<cj->_connect) {
				if(!ports[c]) break;
				++c;
			}
			if (jack_connect (_client, jack_port_name(_jack_port), ports[c])) {
				error(bDANGER, "cannot connect " + d->long_label() + " to port #" + to_string(c) + "\n");
			} else {
				error(bTRACE, "connected " + d->long_label() + " to " + std::string(ports[c]) + "\n");
			}
			free (ports);
		}
	}
}
/*--------------------------------------------------------------------------*/
void DEV_JACK::precalc_last()
{
  ELEMENT::precalc_last();
  _samples_per_step = (short unsigned) (double(_sr) * CKT_BASE::_sim->_dtmin / 0.9999);
  error(bTRACE, "precalc_last sr " + to_string(unsigned(_sr)) + "\n");
  error(bTRACE, "precalc_last dtmin " + to_string(CKT_BASE::_sim->_dtmin) + "\n");
  error(bTRACE, "precalc_last samples_per_step " + to_string(_samples_per_step) + "\n");
}
/*--------------------------------------------------------------------------*/
int DEV_JACK::process_cb(jack_nframes_t nframes, void* ctx)
{
	unsigned ret = 0;
	DEV_JACK** c = (DEV_JACK**) ctx;
	while(*c) {
		ret += (*c)->process(nframes);
		++c;
	}
	return ret;
}
/*--------------------------------------------------------------------------*/
int DEV_JACK::process(jack_nframes_t nframes)
{
	DEV_JACK* j = this;
	assert(_nframes==nframes);

	if (_pos < nframes){
		error(bWARNING, "jack lag %i of %i\n", _pos, nframes);
	} else {
		// cerr << "." << _buffer[13];
	}
	sample_t *buffer = (sample_t *) jack_port_get_buffer (_jack_port, nframes);

	if (jack_port_flags(_jack_port) & JackPortIsOutput) {
		memcpy (buffer, _buffer, sizeof (sample_t) * nframes);
	} else {
		memcpy (j->_buffer, buffer, sizeof (sample_t) * nframes);
	}

	_pos = 0;
	return 0;
}
/*--------------------------------------------------------------------------*/
int DEV_JACK::srate_cb(jack_nframes_t nframes, void *ctx)
{
	unsigned ret = 0;
	DEV_JACK** c = (DEV_JACK**) ctx;
	while(*c) {
		ret += (*c)->srate(nframes);
		++c;
	}
	return ret;
}
/*--------------------------------------------------------------------------*/
int DEV_JACK::srate(jack_nframes_t nframes)
{
	error(bDANGER, "srate %i\n", nframes);
	incomplete();
	return 0;
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
EVAL_BM_JACK bmjack(CC_STATIC);
DISPATCHER<COMMON_COMPONENT>::INSTALL d1(&bm_dispatcher, "jack", &bmjack);
/*--------------------------------------------------------------------------*/
#include "d_to_jack.cc"
#include "d_from_jack.cc"
