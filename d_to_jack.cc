/*$Id: d_meter.cc,v 26.138 2013/04/24 02:44:30 al Exp $ -*- C++ -*-
 * Copyright (C) 2010 Albert Davis
 * Author: Albert Davis <aldavis@gnu.org>
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 * to jack device
 * does nothing to the circuit, but has probes
 */

#include <globals.h>
#include "e_jack.h"
#include <u_xprobe.h>
/*--------------------------------------------------------------------------*/
namespace {
/*--------------------------------------------------------------------------*/
class DEV_MEAS_JACK : public DEV_JACK {
public:
	explicit DEV_MEAS_JACK() : DEV_JACK() {
		COMMON_COMPONENT* b = &bmjack;
		assert(b);
		COMMON_COMPONENT* bc = b->clone();
		attach_common(bc);
		set_dev_type("m_jack");
		// assert(d->dev_type() == "m_jack"); not yet
	}
private:
	explicit DEV_MEAS_JACK(DEV_MEAS_JACK const& j) : DEV_JACK(j) {
	}
public:
  CARD* clone()const override { return new DEV_MEAS_JACK(*this); }
  bool print_type_in_spice()const override {return false;}
  char	   id_letter()const override	{return '\0';}
  std::string value_name()const override {return "";}
  std::string dev_type()const override	{return "meter";}
  int	   max_nodes()const override	{return 2;}
  int	   min_nodes()const override	{return 2;}
  int	   matrix_nodes()const override	{return 2;}
  int	   net_nodes()const override	{return 2;}
  void	   tr_iwant_matrix() override {}
  void	   ac_iwant_matrix() override {}

  void     precalc_last()override;
  double   tr_involts()const override	{return dn_diff(n_(OUT1).v0(), n_(OUT2).v0());}
  double   tr_involts_limited()const override {return tr_involts();}
  void     tr_accept()override;
  TIME_PAIR tr_review()override{
	  q_accept();
	  return _time_by;
  }

  COMPLEX  ac_involts()const override	{return n_(OUT1)->vac() - n_(OUT2)->vac();}
  double   tr_probe_num(const std::string&)const override;
  XPROBE   ac_probe_ext(const std::string&)const override;

  std::string port_name(int i)const override {
    assert(i >= 0);
    assert(i < 4);
    static std::string names[] = {"outp", "outn", "inp", "inn"};
    return names[i];
  }
}p1;
DISPATCHER<CARD>::INSTALL d1(&device_dispatcher, "to_jack|m_jack|meas_jack", &p1);
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
void DEV_MEAS_JACK::precalc_last()
{
  DEV_JACK::precalc_last();
  set_constant(true);
  set_converged();
}
/*--------------------------------------------------------------------------*/
double DEV_MEAS_JACK::tr_probe_num(const std::string& x)const
{
  if (Umatch(x, "gain ")) {
    return tr_outvolts() / tr_involts();
  }else{
    return DEV_JACK::tr_probe_num(x);
  }
}
/*--------------------------------------------------------------------------*/
XPROBE DEV_MEAS_JACK::ac_probe_ext(const std::string& x)const
{
  if (Umatch(x, "gain ")) {
    return XPROBE(ac_outvolts() / ac_involts());
  }else{
    return DEV_JACK::ac_probe_ext(x);
  }
}
/*--------------------------------------------------------------------------*/
void DEV_MEAS_JACK::tr_accept()
{
	DEV_JACK::tr_accept();
	auto e = prechecked_cast<DEV_JACK*>(this);
	assert(e);
	assert(e->_buffer);

	if (!e->is_source()) {
		while (e->_pos > e->_nframes) {
			// waiting for process to fetch/push
		}
		for(unsigned i=0; i < e->_samples_per_step; ++i){
			e->_buffer[e->_pos++] = (sample_t) e->tr_input();
		}
	} else {
		e->_pos += e->_samples_per_step;
	}

	// if (!e->_pos) {
	// 	// cerr << ",";
	// }
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
} // namespace
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
