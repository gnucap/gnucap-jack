/*                              -*- C++ -*-
 * Copyright (C) 2024 Felix Salfelder
 * Author: Felix Salfelder
 *
 * This file is part of "gnucap-jack".
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 * JACK device
 */
#ifndef E_JACK
#define E_JACK
#include <e_elemnt.h>
#include <jack/types.h>
#include <globals.h>
#include <u_lang.h>
#include <l_denoise.h>
#include <bm.h>
#include "e_jack.h"
/*--------------------------------------------------------------------------*/
static const unsigned MAXPORTS = 16;
/*--------------------------------------------------------------------------*/
typedef jack_default_audio_sample_t sample_t;
/*--------------------------------------------------------------------------*/
const int _default_direction (0);
const int _default_connect (0);
/*--------------------------------------------------------------------------*/
// static int srate(jack_nframes_t nframes, void *ctx);
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
// COMMON_JACK
class EVAL_BM_JACK : public EVAL_BM_ACTION_BASE {
public:
  PARAMETER<int> _direction;
  PARAMETER<int> _connect;
  PARAMETER<double> _end;
public:
  int set_param_by_name(std::string Name, std::string Value)override;

  static std::map<std::string, PARA_BASE EVAL_BM_JACK::*> _param_dict;
  explicit	EVAL_BM_JACK(const EVAL_BM_JACK& p);
public:
  explicit      EVAL_BM_JACK(int c=0);
		~EVAL_BM_JACK()		{}
private: // override vitrual
  bool		operator==(const COMMON_COMPONENT&)const override;
  COMMON_COMPONENT* clone()const override { return new EVAL_BM_JACK(*this);}
  void		print_common_obsolete_callback(OMSTREAM&, LANGUAGE*)const override;

  void		precalc_first(const CARD_LIST*)override;
 // void		precalc_last(const CARD_LIST*);
  void		tr_eval(ELEMENT*)const override;
  TIME_PAIR	tr_review(COMPONENT*)const override;
  std::string	name()const override		{return "jack";}
  bool		ac_too()const override		{return true;}
  bool		parse_numlist(CS&)override;
  bool		parse_params_obsolete_callback(CS&)override;
public:
  // void expand(COMPONENT const* d)override;
};
/*--------------------------------------------------------------------------*/
class DEV_JACK : public ELEMENT {
  mutable node_t _N[4];
public:
  static unsigned long _sr;
  static unsigned _nframes;
  static jack_client_t *_client;
  static unsigned short _samples_per_step;
protected:
  node_t& n_(int i)const override {assert(i<4); return _N[i];}
protected:
public:
  mutable sample_t* _buffer{nullptr};
  mutable volatile unsigned _pos;
  jack_port_t *_jack_port{nullptr};
protected:
  DEV_JACK() : ELEMENT() {}
  DEV_JACK(DEV_JACK const& j) : ELEMENT(j) {}
protected:
  void precalc_last()override;
public:
  int process(jack_nframes_t nframes);
  void expand()override;
private:
  static DEV_JACK* _ctx[MAXPORTS];
private:
  static int process_cb(jack_nframes_t nframes, void* ctx);
  static int srate_cb(jack_nframes_t nframes, void* ctx);
  friend int srate(jack_nframes_t, void*);
  int srate(jack_nframes_t nframes);
};
/*--------------------------------------------------------------------------*/
inline EVAL_BM_JACK::EVAL_BM_JACK(int c)
  :EVAL_BM_ACTION_BASE(c)
{
}
/*--------------------------------------------------------------------------*/
inline EVAL_BM_JACK::EVAL_BM_JACK(const EVAL_BM_JACK& p)
  :EVAL_BM_ACTION_BASE(p),
	_direction(p._direction),
	_connect(p._connect)
{
}
/*--------------------------------------------------------------------------*/
inline bool EVAL_BM_JACK::operator==(const COMMON_COMPONENT& x)const
{
  const EVAL_BM_JACK* p = dynamic_cast<const EVAL_BM_JACK*>(&x);
  bool rv = p
    && _connect == p->_connect
    && _direction == p->_direction;
  return rv;
}
/*--------------------------------------------------------------------------*/
inline void EVAL_BM_JACK::print_common_obsolete_callback(OMSTREAM& o, LANGUAGE* lang)const
{
  incomplete(); // obsolete.
  assert(lang);
  o << name();
  print_pair(o, lang, "direction",  _direction);
  EVAL_BM_ACTION_BASE::print_common_obsolete_callback(o, lang);
}
/*--------------------------------------------------------------------------*/
inline void EVAL_BM_JACK::precalc_first(const CARD_LIST* Scope)
{
  assert(Scope);
  EVAL_BM_ACTION_BASE::precalc_first(Scope);
  _direction.e_val(_default_direction, Scope);
  _connect.e_val(_default_connect, Scope);
}
/*--------------------------------------------------------------------------*/
// }
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
inline TIME_PAIR EVAL_BM_JACK::tr_review(COMPONENT* d)const
{
  d->q_accept();
#ifdef HAVE_DTIME
  return d->_dt_by;
#else
  return d->_time_by;
#endif
}
/*--------------------------------------------------------------------------*/
inline bool EVAL_BM_JACK::parse_numlist(CS& cmd)
{
	incomplete();
  unsigned start = cmd.cursor();
#if 0
  unsigned here = cmd.cursor();
  for (PARAMETER<double>* i = &_direction;  i < &_end;  ++i) {
    PARAMETER<double> val(NOT_VALID);
    cmd >> val;
    if (cmd.stuck(&here)) {
      break;
    }else{
      *i = val;
    }
  }
#endif
  return cmd.gotit(start);
}
/*--------------------------------------------------------------------------*/
inline bool EVAL_BM_JACK::parse_params_obsolete_callback(CS& cmd)
{
  return ONE_OF
    || Get(cmd, "dir{ection}",	&_direction)
    || EVAL_BM_ACTION_BASE::parse_params_obsolete_callback(cmd)
    ;
}
/*--------------------------------------------------------------------------*/
inline int EVAL_BM_JACK::set_param_by_name(std::string Name, std::string Value)
{
  if(Name == "direction") {
    _direction = Value;
    return 1;
  }else if(Name == "dir") {
    _direction = Value;
    return 1;
  }else if(Name == "connect") {
    _connect = Value;
    return 2;
  }else{ untested();
    return EVAL_BM_ACTION_BASE::set_param_by_name(Name, Value);
  }
}
/*--------------------------------------------------------------------------*/
inline void EVAL_BM_JACK::tr_eval(ELEMENT* E) const
{
  auto d = prechecked_cast<DEV_JACK const*>(E);
  double ev = 0;

  assert (d->is_source());

  while (d->_pos > d->_nframes) {
    // waiting for process to fetch/push
  }
  for (unsigned i=d->_pos; i<d->_pos+d->_samples_per_step; ++i) {
    ev += d->_buffer[i];
  }
  ev /= (double)d->_samples_per_step;

 // const EVAL_BM_JACK* p = dynamic_cast<const EVAL_BM_JACK*>(common());
 // p->
  tr_finish_tdv(E, ev);
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
#endif
// vim:ts=8:sw=2:noet:
